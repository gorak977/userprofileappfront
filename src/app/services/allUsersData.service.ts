import { Injectable } from "@angular/core";

import { UserShort } from "../interfaces/user.model";


@Injectable({ providedIn: "root" })
export class AllUsersDataService {
  private users: UserShort[] = [];

  constructor() {}

  getUsers() {
    return this.users;
  }
  setUser(user: UserShort) {
    this.users.push(user);
  }
  clearUsers() {
    this.users = [];
  }
  deleteUser(id: number) {
    const index: number = this.users.indexOf(
      this.users.find(el => el.id == id)
    );
    if (index !== -1) {
      this.users.splice(index, 1);
    }
  }
}
