import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "src/environments/environment";
import { AllUsersDataService } from "./allUsersData.service";
import { UserFull, UserFullToRequest } from "../interfaces/user.model";
import { UserDetailDataService } from "./userDetailData.service";
import { DatePipe } from "@angular/common";
import { Avatar } from "../interfaces/avatar.model";
import { Role } from "../interfaces/role.model";

@Injectable({ providedIn: "root" })
export class UserDetailRequestService {
  private apiUrl = environment.apiUrl + "User";

  constructor(
    private http: HttpClient,
    private userDetailDataService: UserDetailDataService,
    private allUsersDataService: AllUsersDataService,
    public datepipe: DatePipe
  ) {}

  getUserDetail(id: number) {
    this.http.get(this.apiUrl + "/" + id).subscribe(
      (user: UserFull) => {
        if (user) {
          this.http
            .get(environment.apiUrl + "Role/" + user.roleID)
            .subscribe((role: Role) => {
              this.userDetailDataService.setRole({
                id: role.id,
                roleName: role.roleName
              });
            });
          if (user.avatarID != null) {
            this.http
              .get(environment.apiUrl + "Avatar/" + user.avatarID)
              .subscribe((avatar: Avatar) => {
                console.log(avatar);
                if (avatar != null) {
                  this.userDetailDataService.setAvatar({
                    ID: avatar.ID,
                    image: avatar.image
                  });
                }
              });
          } else {
            this.userDetailDataService.setAvatar(null);
          }
          this.userDetailDataService.setUser({
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            middleName: user.middleName,
            birthDate: this.datepipe.transform(
              new Date(user.birthDate),
              "d MMMM, y"
            ),
            description: user.description,
            avatarID: user.avatarID,
            roleID: user.roleID
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  addUser(user: UserFullToRequest) {
    console.log(user);
    console.log(JSON.stringify(user));
    this.http
      .post(this.apiUrl, JSON.stringify(user), {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .subscribe(
        (data: any) => {
          this.userDetailDataService.setResults({
            isError: false,
            message: "User was added to database successfuly"
          });
        },
        error => {
          this.userDetailDataService.setResults({
            isError: true,
            message: error
          });
        }
      );
  }

  deleteUser(userID: number, callBack) {
      this.http.delete(this.apiUrl + "/delete/" + userID.toString()).subscribe(
        ok => {  
          this.allUsersDataService.deleteUser(userID);
          this.userDetailDataService.setUser(null);
          callBack();
        },
        err => {
          console.log(err);
        }
      );
  }
}
