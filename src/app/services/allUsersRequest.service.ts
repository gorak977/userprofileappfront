import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from "@angular/common/http";

import { environment } from "src/environments/environment";
import { AllUsersDataService } from "./allUsersData.service";
import {
  UserFull,
  UserFullToRequest,
  UserShort
} from "../interfaces/user.model";

@Injectable({ providedIn: "root" })
export class AllUsersRequestService {
  private apiUrl = environment.apiUrl + "User";

  constructor(
    private http: HttpClient,
    private allUsersDataService: AllUsersDataService
  ) {}

  getAllUsers() {
    this.http.get(this.apiUrl).subscribe(
      (users: UserShort[]) => {
        if (users) {
          this.allUsersDataService.clearUsers();
          users.forEach(user => {
            this.allUsersDataService.setUser({
              id: user.id,
              fullName: user.fullName,
              age: user.age,
              role: user.role
            });
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
