import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import { environment } from "src/environments/environment";
import { RolesDataService } from "./rolesData.service";
import { Role } from '../interfaces/role.model';


@Injectable({ providedIn: "root" })
export class RolesRequestService {
  private apiUrl = environment.apiUrl + "Role";

  constructor(
    private http: HttpClient,
    private rolesDataService: RolesDataService
  ) {}

  getRoles() {
    this.http.get(this.apiUrl).subscribe(
      (roles: Role[]) => {
        if (roles) {
          this.rolesDataService.clearRoles();
          roles.forEach(role => {
            this.rolesDataService.setRole({id: role.id, roleName: role.roleName});
          });
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
