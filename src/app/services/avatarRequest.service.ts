import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpEventType
} from "@angular/common/http";

import { environment } from "src/environments/environment";
import { UserDetailDataService } from "./userDetailData.service";
import { Avatar } from "../interfaces/avatar.model";

@Injectable({ providedIn: "root" })
export class AvatarRequestService {
  private apiUrl = environment.apiUrl + "Avatar";

  constructor(
    private http: HttpClient,
    private userDetailData: UserDetailDataService
  ) {}

  sendFile(img: File, callback) {
    const fd = new FormData();
    var id = 0;
    fd.append("image", img);
    // let promise = new Promise(() => {
    //   this.http
    //     .post(this.apiUrl, fd)
    //     .toPromise()
    //     .then((data: any) => {
    //       console.log(data.avatar);
    //       this.userDetailData.setAvatar({
    //         ID: data.avatar.id,
    //         image: data.avatar.image
    //       });
    //       console.log(1);
    //     })
    //     .then(() => {
    //       console.log(2);
    //     });
    // });
    this.http.post(this.apiUrl, fd).subscribe(
      (data: any) => {
        console.log(data.avatar);
        this.userDetailData.setAvatar({
          ID: data.avatar.id,
          image: data.avatar.image
        });
        callback();
      },
      error => {
        console.log(error);
      }
    );
  }
}
