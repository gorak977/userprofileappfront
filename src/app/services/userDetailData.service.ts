import { Injectable } from "@angular/core";

import { UserShort, UserFull } from "../interfaces/user.model";
import { Avatar } from "../interfaces/avatar.model";
import { RequestResults } from '../interfaces/requestResults.model';
import { Role } from '../interfaces/role.model';


@Injectable({ providedIn: "root" })
export class UserDetailDataService {
  private user: UserFull;
  private userRole: Role;
  private avatar: Avatar = null;

  public requestResults: RequestResults;

  constructor() {}

  getUser() {
    return this.user;
  }
  setUser(user: UserFull) {
    this.user = user;
  }
  getRole() {
    return this.userRole;
  }
  setRole(role: Role) {
    this.userRole = role;
  }

  getAvatar() {
    return this.avatar;
  }
  setAvatar(avatar: Avatar) {
    this.avatar = avatar;
  }

  setResults(results: RequestResults){
      this.requestResults = results
  }

  clearResults(){
    this.requestResults = null;
  }
}
