import { Injectable } from "@angular/core";

import { Role } from '../interfaces/role.model';


@Injectable({ providedIn: "root" })
export class RolesDataService {
  private roles: Role[] = [];

  constructor() {}

  getRoles() {
    return this.roles;
  }
  setRole(role: Role) {
    this.roles.push(role);
  }
  clearRoles(){
      this.roles = [];
  }
}
