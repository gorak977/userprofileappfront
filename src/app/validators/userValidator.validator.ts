import { FormControl, AbstractControl } from "@angular/forms";
import { DatePipe } from "@angular/common";

export class UserValidator {
  userNameValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    const accountRgEx: RegExp = /^(?=.{2,50}$)[A-Z][a-z]+$/;
    const valid = accountRgEx.test(control.value);
    return valid ? null : { name: true };
  }

  userBirthDateValidator(
    control: AbstractControl
  ): { [key: string]: boolean } | null {
    let birthDate = new Date(control.value);
    let timeDiff = Math.abs(Date.now() - birthDate.getTime());
    let age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    return age > 17 ? null : { age: true };
  }
}
