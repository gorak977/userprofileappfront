export interface RequestResults{
    isError: boolean,
    message: string
}