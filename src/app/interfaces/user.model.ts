export interface UserShort {
  id: number;
  fullName: string;
  age: number;
  role: string;
}

export interface UserFull {
  id: number;
  firstName: string;
  lastName: string;
  middleName: string;
  birthDate: string;
  description: string;
  avatarID: number;
  roleID: number;
}

export interface UserFullToRequest{
  firstName: string;
  lastName: string;
  middleName: string;
  birthDate: string;
  description: string;
  avatarID: number;
  roleID: number;
}
