import { Component, OnInit } from "@angular/core";
import { DatePipe } from "@angular/common";

import { UserDetailDataService } from "src/app/services/userDetailData.service";
import { UserDetailRequestService } from "src/app/services/userDetailRequest.service";
import { AllUsersComponent } from "../all-users/all-users.component";
import { AllUsersDataService } from "src/app/services/allUsersData.service";
import { AllUsersRequestService } from "src/app/services/allUsersRequest.service";
import { environment } from "src/environments/environment";


@Component({
  selector: "app-user-details",
  templateUrl: "./user-details.component.html",
  styleUrls: ["./user-details.component.scss"]
})
export class UserDetailsComponent implements OnInit {
  defaultUserAvatarURL = "../../../assets/images/default-user-avatar.png";
 
  constructor(
    public userDetailDataService: UserDetailDataService,
    private userDetailRequestService: UserDetailRequestService,
    private allUsersDataService: AllUsersDataService,
    private allUsersRequestService: AllUsersRequestService,
    public datepipe: DatePipe
  ) {}

  ngOnInit() {}
}
