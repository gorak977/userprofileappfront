import { Component, OnInit, HostListener } from "@angular/core";


@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  isNavigation: boolean = false;
  innerWidth: number;

  constructor() {}

  ngOnInit() {
    this.innerWidth = window.innerWidth;
  }

  toggleNavigation() {
    this.isNavigation = !this.isNavigation;
  }
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
}
