import { Component, OnInit } from "@angular/core";

import { AllUsersRequestService } from "src/app/services/allUsersRequest.service";
import { AllUsersDataService } from "src/app/services/allUsersData.service";
import { UserShort } from "src/app/interfaces/user.model";
import { UserDetailRequestService } from "src/app/services/userDetailRequest.service";
import { UserDetailDataService } from "src/app/services/userDetailData.service";


@Component({
  selector: "app-all-users",
  templateUrl: "./all-users.component.html",
  styleUrls: ["./all-users.component.scss"]
})
export class AllUsersComponent implements OnInit {
  constructor(
    private allUsersRequestService: AllUsersRequestService,
    public allUsersData: AllUsersDataService,
    private userDetailRequestService: UserDetailRequestService,
    private userDetailDataService: UserDetailDataService
  ) {}

  ngOnInit() {
    this.allUsersRequestService.getAllUsers();
  }

  onSelect(user: UserShort) {
    this.userDetailRequestService.getUserDetail(user.id);
  }

  onDeleteUser(user: UserShort) {
    this.userDetailRequestService.deleteUser(user.id, () => {
      this.ngOnInit();
    });
  }
}
