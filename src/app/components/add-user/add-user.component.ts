import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ImageCropperComponent, CropperSettings } from "ngx-img-cropper";

import { RolesRequestService } from "src/app/services/rolesRequest.service";
import { RolesDataService } from "src/app/services/rolesData.service";
import { AvatarRequestService } from "src/app/services/avatarRequest.service";
import { UserDetailDataService } from "src/app/services/userDetailData.service";
import { UserValidator } from "src/app/validators/userValidator.validator";
import { AllUsersDataService } from "src/app/services/allUsersData.service";
import { UserDetailRequestService } from "src/app/services/userDetailRequest.service";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
  userForm: FormGroup;
  selectedFile: File = null;
  errorMessage: string = null;
  imgUrl = environment.defaultUserAvatarURL;
  cropperSettings: CropperSettings;
  imageCropData: any;
  isCropper: boolean = false;
  @ViewChild("cropper", undefined)
  cropper: ImageCropperComponent;

  constructor(
    private rolesService: RolesRequestService,
    public roleDataService: RolesDataService,
    private userRequestService: UserDetailRequestService,
    public allUserDataService: AllUsersDataService,
    private avatarRequestService: AvatarRequestService,
    public userDetailDataSerice: UserDetailDataService,
    private userValidator: UserValidator
  ) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.width = 256;
    this.cropperSettings.height = 256;
    this.cropperSettings.croppedWidth = 256;
    this.cropperSettings.croppedHeight = 256;
    this.cropperSettings.canvasWidth = 200;
    this.cropperSettings.canvasHeight = 200;
    this.imageCropData = {};
  }

  ngOnInit() {
    this.userDetailDataSerice.requestResults = null;
    this.rolesService.getRoles();

    this.userForm = new FormGroup({
      firstname: new FormControl("", [
        Validators.required,
        Validators.maxLength(50),
        this.userValidator.userNameValidator
      ]),
      lastname: new FormControl("", [
        Validators.required,
        Validators.maxLength(50),
        this.userValidator.userNameValidator
      ]),
      middlename: new FormControl(""),
      birthdate: new FormControl("2000-01-01", [
        Validators.required,
        this.userValidator.userBirthDateValidator
      ]),
      description: new FormControl(""),
      role: new FormControl(1, Validators.required),
      avatar: new FormControl("")
    });
  }

  onFileSelect(event) {
    let reader = new FileReader();
    let fileToUpload = event.target.files[0];
    console.log(fileToUpload);
    if (fileToUpload != undefined) {
      let fileSize = new Number(fileToUpload.size);
      if (fileSize < 1024 * 1024 * 10) {
        this.errorMessage = null;
        reader.readAsDataURL(fileToUpload);
        this.selectedFile = fileToUpload;
      } else {
        this.selectedFile = null;
        this.imgUrl = environment.defaultUserAvatarURL;
        this.errorMessage =
          "Too large file. Size=" +
          (
            Math.round((fileSize.valueOf() / (1024 * 1024)) * 100) / 100
          ).toString() +
          "MB. Maximum accepted size is 10MB";
      }
      reader.onload = (event: any) => {
        this.imgUrl = <string>reader.result;
      };
      reader.onerror = (event: any) => {
        console.log("File could not be read");
      };
    }
  }

  onClearAvatar() {
    this.isCropper = false;
    this.selectedFile = null;
    this.imgUrl = environment.defaultUserAvatarURL;
  }

  onToggleCropper() {
    this.isCropper = !this.isCropper;
    if (this.isCropper === true && this.cropper != undefined) {
      let image = new Image();
      image.src = this.imgUrl;
      this.cropper.setImage(image);
    } else {
      fetch(this.imageCropData.image)
        .then(res => res.blob())
        .then(blob => {
          var reader = new FileReader();
          reader.readAsDataURL(blob);
          this.selectedFile = <File>blob;
          reader.onload = (event: any) => {
            this.imgUrl = <string>reader.result;
          };
        });
    }
  }

  onAddUser() {
    if (this.selectedFile) {
      this.avatarRequestService.sendFile(this.selectedFile, () => {
        this.userRequestService.addUser({
          firstName: this.userForm.get("firstname").value,
          lastName: this.userForm.get("lastname").value,
          middleName: this.userForm.get("middlename").value,
          birthDate: this.userForm.get("birthdate").value,
          description: this.userForm.get("description").value,
          avatarID: this.userDetailDataSerice.getAvatar().ID,
          roleID: Number(this.userForm.get("role").value)
        });
      });
    } else {
      this.userRequestService.addUser({
        firstName: this.userForm.get("firstname").value,
        lastName: this.userForm.get("lastname").value,
        middleName: this.userForm.get("middlename").value,
        birthDate: this.userForm.get("birthdate").value,
        description: this.userForm.get("description").value,
        avatarID: null,
        roleID: Number(this.userForm.get("role").value)
      });
    }
  }
}
