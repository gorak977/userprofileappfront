import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";
import { DatePipe } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./components/app/app.component";
import { HeaderComponent } from "./components/header/header.component";
import { AddUserComponent } from "./components/add-user/add-user.component";
import { AllUsersComponent } from "./components/all-users/all-users.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { UserDetailsComponent } from "./components/user-details/user-details.component";
import { UserValidator } from "./validators/userValidator.validator";
import { ImageCropperComponent } from "ngx-img-cropper";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DemoMaterialModule } from "./material-module";


const appRoutes: Routes = [
  //{ path: "", component: StartPageComponent },
  { path: "form", component: AddUserComponent },
  { path: "list", component: AllUsersComponent },
  { path: "**", component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AddUserComponent,
    AllUsersComponent,
    NotFoundComponent,
    UserDetailsComponent,
    ImageCropperComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule
  ],
  providers: [DatePipe, UserValidator],
  bootstrap: [AppComponent]
})
export class AppModule {}
