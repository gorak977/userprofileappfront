export const environment = {
  production: true,
  apiUrl: "http://localhost:5000/",
  defaultUserAvatarURL: "../assets/images/default-user-avatar.png"
};
